# R - First program

This tutorial aims to introduce programming with R.

It covers the opening of flat files, some data management operations and the generation of graphics.

It does not require additional packages.

You will learn how to :

- Organize the script
- Document the script
- Load the flat files
- Describe the datasets
- Format dates
- Compute new variables
- Generate graphics
test